FROM ubuntu:latest
LABEL version="1"
LABEL description="Reloj en JS."

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install nginx -y

# La instruccion ADD es parecida COPY
# ambos copian un fichero hacia un destino
# Pero ADD obtiene ese fichero de una url
ADD https://gitlab.com/sergio.pernas1/js-clock/-/raw/main/index.html /var/www/html
RUN chmod +r /var/www/* -R
ADD https://gitlab.com/sergio.pernas1/js-clock/-/raw/main/default /etc/nginx/sites-available
ADD https://gitlab.com/sergio.pernas1/js-clock/-/raw/main/entrypoint.sh /
RUN chmod +x /entrypoint.sh

ENV SITENAME appclock

EXPOSE 80 443

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]









